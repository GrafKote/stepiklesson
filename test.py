class Dijkstra:

    def __init__(self, vertices, graph):
        self.vertices = vertices  # ("A", "B", "C" ...)
        self.graph = graph  # {"A": {"B": 1}, "B": {"A": 3, "C": 5} ...}

    def find_route(self, start, end):
        unvisited = {n: float("inf") for n in self.vertices}
        unvisited[start] = 0  # set start vertex to 0
        visited = {}  # list of all visited nodes
        parents = {}  # predecessors
        while unvisited:
            min_vertex = min(unvisited, key=unvisited.get)  # get smallest distance
            for neighbour, _ in self.graph.get(min_vertex, {}).items():
                if neighbour in visited:
                    continue
                new_distance = unvisited[min_vertex] + self.graph[min_vertex].get(neighbour, float("inf"))
                if new_distance < unvisited[neighbour]:
                    unvisited[neighbour] = new_distance
                    parents[neighbour] = min_vertex
            visited[min_vertex] = unvisited[min_vertex]
            unvisited.pop(min_vertex)
            if min_vertex == end:
                break
        return parents, visited

    @staticmethod
    def generate_path(parents, start, end):
        path = [end]
        while True:
            key = parents[path[0]]
            path.insert(0, key)
            if key == start:
                break
        return path



# Вершины графа
#
# A -5- B -4- C
# | \         |
# 4   1       2
# |    \      |
# D -1- E -9- Z
#

input_vertices = ("A", "B", "C", "D", "E", "Z")
# Пути и веса
input_graph = {
    "A": {"B": 5, "D": 4, "E": 1},
    "B": {"A": 5, "C": 4},
    "C": {"B": 4, "Z": 2},
    "D": {"A": 4, "E": 1},
    "E": {"D": 1, "Z": 9, "E": 1},
    "Z": {"E": 9, "C": 2},
}
start_vertex = "A"
end_vertex = "Z"

# Инициализация класса
dijkstra = Dijkstra(input_vertices, input_graph)

# Минимальная длина маршрута
p, v = dijkstra.find_route(start_vertex, end_vertex)
print("Distance from %s to %s is: %.2f" % (start_vertex, end_vertex, v[end_vertex]))
# Минимальный путь
se = dijkstra.generate_path(p, start_vertex, end_vertex)
print("Path from %s to %s is: %s" % (start_vertex, end_vertex, " -> ".join(se)))