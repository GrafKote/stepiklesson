#in_var = '4 8 0 3 4 2 0 3'
#in_var = '10'
#in_var = '1 1 1 1 1 2 2 2'

in_var = input()
var_list = [int(i) for i in in_var.split(" ")]
var_list.sort()


d = {}
for x in var_list:
    d[x] = d.get(x, 0) + 1
out_list = [x for x in var_list if d[x] > 1]
print(*(list(set(out_list))))