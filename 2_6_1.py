total_sum = int(input())
total_cv_sum = total_sum ** 2

while total_sum != 0:
    new_int = int(input())
    new_cv_int = new_int ** 2
    total_cv_sum += new_cv_int
    total_sum += new_int

print(total_cv_sum)